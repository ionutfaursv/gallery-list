package com.example.ionut.androidtutorial;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ionut.androidtutorial.app.GL;

/**
 * Created by ionutfaur on 06.07.2015.
 */
public class Settings {

    private static final String SP_NAME = "settings";

    private static final String KEY_SESSION_ID = "session.id";

    private static Settings instance;

    private SharedPreferences preferences;

    Settings() {
        preferences = GL.getInstance().getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }

        return instance;
    }

    public void setSessionId(String sessionId) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_SESSION_ID, sessionId);
        editor.commit();
    }

    public String getSessionId() {
        return preferences.getString(KEY_SESSION_ID, null);
    }

    public void clear() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
