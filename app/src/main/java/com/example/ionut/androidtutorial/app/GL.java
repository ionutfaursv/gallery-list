package com.example.ionut.androidtutorial.app;

import android.app.Application;

/**
 * Created by ionutfaur on 06.07.2015.
 */
public class GL extends Application {

    private static GL instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static GL getInstance() {
        return instance;
    }

}
