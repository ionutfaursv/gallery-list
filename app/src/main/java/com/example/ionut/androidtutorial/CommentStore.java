package com.example.ionut.androidtutorial;

import android.util.Log;

import com.example.ionut.androidtutorial.beans.Comment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;

/**
 * Created by ionutfaur on 08.07.2015.
 */
public class CommentStore {

    private static CommentStore instance;
    private Set<Comment> comments;

    CommentStore() {
        comments = new HashSet<>();
    }

    public static synchronized CommentStore getInstance() {
        if (instance == null) {
            instance = new CommentStore();
        }

        return instance;
    }

    public void add(Comment comment) {
        comments.add(comment);
    }

    public List get(String fileName) {
        List<Comment> filtered = new ArrayList<>();

        for(Comment comment : comments) {
            if (comment.getFileName().equals(fileName)) {
                filtered.add(comment);
            }
        }

        Collections.sort(filtered, new Comparator<Comment>() {
            @Override
            public int compare(Comment s1, Comment s2) {
                return s1.getText().compareToIgnoreCase(s2.getText());
            }
        });

        return filtered;
    }

    public Integer getLastId(){
        List<Integer> filtered = new ArrayList<>();

        for(Comment comment : comments) {
            filtered.add(comment.getId());
        }

        int max = Integer.MIN_VALUE;
        for(int i = 0; i < filtered.size(); i++){
            if(filtered.get(i) > max){
                max = filtered.get(i);
            }
        }

        return max;
    }

    // doar pt teste
    public List getAll() {
        List<Integer> filtered = new ArrayList<>();

        for(Comment comment : comments) {
            filtered.add(comment.getId());
        }

        return filtered;
    }
}
