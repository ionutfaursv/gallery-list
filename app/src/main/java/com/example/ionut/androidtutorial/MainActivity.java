package com.example.ionut.androidtutorial;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ionut.androidtutorial.beans.Comment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, Handler.Callback {
    public static final String QUERY_URL = "http://192.168.114.92/all/index.php?user=";
    public static final int CHANGE_LAYOUT = 2;
    public static final int WHAT_MAKE_TEXT = 1;

    private EditText username, password;
    private Handler handler;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler(this);

        button = (Button) findViewById(R.id.login);
        button.setOnClickListener(this);

        if (Settings.getInstance().getSessionId() != null) {
            showList();
        }
    }

    private void showList() {
        requestComments();

        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void onClick(View v) {
        Runnable loginRunnable = new Runnable() {
            @Override
            public void run() {
                username = (EditText) findViewById(R.id.username);
                password = (EditText) findViewById(R.id.password);

                String user = username.getText().toString();
                String pass = password.getText().toString();

                if (user.length() > 0 && pass.length() > 0) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://192.168.114.92/login/index.php");

                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    pairs.add(new BasicNameValuePair("user", user));
                    pairs.add(new BasicNameValuePair("pass", pass));
                    try {
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException();
                    }

                    try {
                        HttpResponse response = client.execute(post);
                        String user_id = responseToJSON(response, "data");
                        if (user_id != null) {
                            Settings.getInstance().setSessionId(user_id);

                            Message msg = new Message();
                            msg.what = CHANGE_LAYOUT;
                            msg.obj = "Successfully logged in!";
                            handler.sendMessage(msg);
                        } else {
                            handlerToast("Wrong credentials");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    handlerToast("All field are required");
                }
            }
        };

        Thread loginThread = new Thread(loginRunnable);
        loginThread.start();
    }

    public void requestComments() {
        String urlString = "";

        try {
            urlString = URLEncoder.encode(Settings.getInstance().getSessionId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
                Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(QUERY_URL + urlString + "&comment=",
                new JsonHttpResponseHandler() {
                    public void onSuccess(JSONObject jsonObject) {
                        try {
                            JSONArray data = jsonObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject each_line = data.getJSONObject(i);

                                Comment comment = new Comment();

                                comment.setId(each_line.getInt("id"));
                                comment.setUser(each_line.getInt("user_id"));
                                comment.setText(each_line.getString("comment"));
                                comment.setFileName(each_line.getString("file_name"));

                                CommentStore.getInstance().add(comment);
//                                    Log.d("XX", comment1.getFileName().toString() + " == " + comment1.getId());
                            }
//                            handlerToast("GATA");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
                            Toast.makeText(MainActivity.this, statusCode + " - " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private String responseToJSON(HttpResponse response, String field) {
        String message = null;
        StringBuilder builder = new StringBuilder();
        HttpEntity entity = response.getEntity();
        InputStream content;
        try {
            content = entity.getContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String readJSON = builder.toString();
        try {
            JSONObject jsonObject = new JSONObject(readJSON);
            if (!jsonObject.get(field).equals(null)) {
                message = jsonObject.getString(field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return message;
    }


    @Override
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case WHAT_MAKE_TEXT:
                if (username != null && password != null) {
                    username.setText("");
                    password.setText("");
                }

                String text = (String) message.obj;
                Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
                return true;
            case CHANGE_LAYOUT:
                text = (String) message.obj;
                Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();

                showList();
                return true;
        }

        return false;
    }

    public void handlerToast(String string) {
        Message msg = new Message();
        msg.what = WHAT_MAKE_TEXT;
        msg.obj = string;
        handler.sendMessage(msg);
    }


}
