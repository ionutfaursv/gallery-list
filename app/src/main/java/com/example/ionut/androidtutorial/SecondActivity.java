package com.example.ionut.androidtutorial;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.ionut.androidtutorial.beans.Comment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SecondActivity extends ActionBarActivity implements Handler.Callback{
    public static final int WHAT_MAKE_TEXT = 1;
    public static final String QUERY_URL = "http://192.168.114.92/all/index.php?user=";

    private ImageAdapter adapter;
    private CommentAdapter commentAdapter;
    private ViewMode mode;
    private EditText comment;
    private Handler handler;
    private ViewSwitcher viewSwitcher;
    private Animation slide_in_left, slide_out_right;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        handler = new Handler(this);
        String[] projection = new String[] {
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.DATA
        };
        Cursor cursor = MediaStore.Images.Media.query(getContentResolver(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection);
        List<Image> images = new ArrayList<>();
        while (cursor.moveToNext()) {
            Image image = new Image();
            image.setId(cursor.getInt(0));
            image.setDisplayName(cursor.getString(1));
            image.setBitmapPath(cursor.getString(2));
            images.add(image);
        }

        adapter = new ImageAdapter(images);

        showList();

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        slide_in_left = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_out_right);

        viewSwitcher.setInAnimation(slide_in_left);
        viewSwitcher.setOutAnimation(slide_out_right);

        refresher();

        Toast.makeText(this, CommentStore.getInstance().getAll().toString(), Toast.LENGTH_SHORT).show();

        if(Settings.getInstance().getSessionId() != null){
            Toast.makeText(this, Settings.getInstance().getSessionId().toString() + " SESSION ID", Toast.LENGTH_SHORT).show();
//            Toast.makeText(this, Integer.toString(CommentStore.getInstance().getLastId()) + " LAST ID", Toast.LENGTH_SHORT).show();
        }
    }

    public void refresher(){
        int delay = 20000;
        int period = 20000;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                handlerToast(Integer.toString(CommentStore.getInstance().getLastId()) + " LAST ID");
                test();
//                requestComments();
            }
        }, delay, period);
    }

    public void test(){
        String urlString = "";
        try {
            urlString = URLEncoder.encode(Settings.getInstance().getSessionId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpURLConnection c = null;
        try {
            URL u = new URL(QUERY_URL + urlString + "&comment=" + Integer.toString(CommentStore.getInstance().getLastId()));
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-type", "application/json");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                        Log.d("XCX", sb.toString());
                    }
                    try {
                        JSONObject jsonObj = new JSONObject(sb.toString());
//                        Log.d("ZZZ", jsonObj.getString("message"));
                        JSONArray data = jsonObj.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++){
                            JSONObject each_line = data.getJSONObject(i);
                            Comment comment = new Comment();

                            comment.setId(each_line.getInt("id"));
                            comment.setUser(each_line.getInt("user_id"));
                            comment.setText(each_line.getString("comment"));
                            comment.setFileName(each_line.getString("file_name"));

                            CommentStore.getInstance().add(comment);

                            Log.d("YY", comment.getFileName().toString() + " == " + comment.getId());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    br.close();
                    handlerToast(sb.toString());
                    Log.d("ACI2", sb.toString());
                case 201:
                    return;
                case 401:
                    Log.d("@@", Integer.toString(status) + " status ");
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }


    public void requestComments() {
        String urlString = "";
        try {
            urlString = URLEncoder.encode(Settings.getInstance().getSessionId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
//                Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(QUERY_URL + urlString + "&comment=" + Integer.toString(CommentStore.getInstance().getLastId()),
                new JsonHttpResponseHandler() {
                    public void onSuccess(JSONObject jsonObject) {
                        try {
                            JSONArray data = jsonObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject each_line = data.getJSONObject(i);

                                Comment comment = new Comment();

                                comment.setId(each_line.getInt("id"));
                                comment.setUser(each_line.getInt("user_id"));
                                comment.setText(each_line.getString("comment"));
                                comment.setFileName(each_line.getString("file_name"));

                                CommentStore.getInstance().add(comment);
                                    Log.d("XX", comment.getFileName().toString() + " == " + comment.getId());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
//                            Toast.makeText(SecondActivity.this, statusCode + " - " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void showList() {
        setContentView(R.layout.activity_second);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                viewSwitcher.showNext();
                showBitmap((Image) adapter.getItem(position));
            }
        });
    }

    public void showBitmap(final Image image) {
        ImageView imageView = (ImageView) findViewById(R.id.the_image);
        final Button addButton = (Button) findViewById(R.id.add_button);
        Picasso.with(this).load(image.getBitmapPath()).into(imageView);
        setTitle(image.getDisplayName());

        ListView commentView = (ListView) findViewById(R.id.comments);
        commentAdapter = new CommentAdapter(CommentStore.getInstance().get(image.getDisplayName()));
        commentView.setAdapter(commentAdapter);

        //  add comment
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                postComment(image.getDisplayName());
            }
        });
        mode = ViewMode.IMAGE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_second, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mode == ViewMode.IMAGE) {
            viewSwitcher.showPrevious();
            mode = ViewMode.LIST;
        } else {
            super.onBackPressed();
        }
    }


    static class Image {
        private int id;
        private String displayName;
        private String bitmapPath;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getBitmapPath() {
            return bitmapPath;
        }

        public void setBitmapPath(String bitmapPath) {
            this.bitmapPath = "file:" + bitmapPath;
        }
    }

    class ImageAdapter extends BaseAdapter {

        private List<Image> images;
        private LayoutInflater inflater;

        ImageAdapter(List<Image> images) {
            this.images = images;
            inflater = SecondActivity.this.getLayoutInflater();
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public Object getItem(int position) {
            return images.get(position);
        }

        @Override
        public long getItemId(int position) {
            return images.get(position).getId();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.image_view, null);
            }

            Image image = images.get(position);

            TextView tvDisplayName = (TextView) convertView.findViewById(R.id.tv_display_name);
            tvDisplayName.setText(image.getDisplayName());

            TextView tvDisplayId = (TextView) convertView.findViewById(R.id.tv_display_id);
            tvDisplayId.setText(Integer.toString(image.getId()));

            ImageView ivThumb = (ImageView) convertView.findViewById(R.id.iv_thumb);
            Picasso.with(SecondActivity.this).load(image.getBitmapPath()).into(ivThumb);

            return convertView;
        }
    }

    class CommentAdapter extends BaseAdapter {

        private List<Comment> comment;
        private LayoutInflater inflater;

        CommentAdapter(List<Comment> comment) {
            this.comment = comment;
            inflater = SecondActivity.this.getLayoutInflater();
        }

        @Override
        public int getCount() {
            return comment.size();
        }

        @Override
        public Object getItem(int position) {
            return comment.get(position);
        }

        @Override
        public long getItemId(int position) {
            return comment.get(position).getId();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.coment_view, null);
            }

            Comment comments = comment.get(position);

            TextView displayCommentId = (TextView) convertView.findViewById(R.id.id);
            displayCommentId.setText(" "+ comments.getId());

            TextView displayComment = (TextView) convertView.findViewById(R.id.file_name);
            displayComment.setText(comments.getText());

            return convertView;
        }
    }

    enum ViewMode {
        LIST,
        IMAGE
    }

    public void postComment(final String name) {

//        try {
//            URL url = new URL("http://192.168.114.92/insert/index.php");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//            conn.setRequestMethod("GET");
//            conn.addRequestProperty("tet", "123");
//            conn.getOutputStream();
//            conn.getInputStream();
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Runnable postRunnable = new Runnable() {
            @Override
            public void run() {
                comment = (EditText) findViewById(R.id.comment);
                String com = comment.getText().toString();

                if (comment.length() > 0) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://192.168.114.92/insert/index.php");

                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    pairs.add(new BasicNameValuePair("user", Settings.getInstance().getSessionId()));
                    pairs.add(new BasicNameValuePair("comment", com));
                    pairs.add(new BasicNameValuePair("file", name));
                    try {
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    try {
                        HttpResponse response = client.execute(post);
                        String result = responseToJSON(response, "data");
                        if(result.equals("true")){
                            handlerToast("Comment has been added");
                        } else {
                            handlerToast("Error");
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    handlerToast("All field are required");
                }
            }
        };

        Thread postThread = new Thread(postRunnable);
        postThread.start();
    }


    private String responseToJSON(HttpResponse response, String field) {
        String message = null;
        StringBuilder builder = new StringBuilder();
        HttpEntity entity = response.getEntity();
        InputStream content;
        try {
            content = entity.getContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String readJSON = builder.toString();
        try {
            JSONObject jsonObject = new JSONObject(readJSON);
            if(!jsonObject.get(field).equals(null)){
                message = jsonObject.getString(field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return message;
    }

    public void logout(){
        Settings.getInstance().clear();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case WHAT_MAKE_TEXT:
                if(comment != null){
                    comment.setText("");
                }

                String text = (String) message.obj;
                Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
                return true;
        }

        return false;
    }

    public void handlerToast(String string){
        Message msg = new Message();
        msg.what = WHAT_MAKE_TEXT;
        msg.obj = string;
        handler.sendMessage(msg);
    }

    //  api

//    public void getTwo(){
//        Runnable r = new Runnable() {
//            @Override
//            public void run() {
//                String urlString = "";
//                try {
//                    urlString = URLEncoder.encode(Setings.getInstance().getKeySessionId(), "UTF-8");
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//
//                HttpURLConnection c = null;
//                try {
//                    URL u = new URL(QUERY_URL + urlString);
//                    c = (HttpURLConnection) u.openConnection();
//                    c.setRequestMethod("GET");
//                    c.setRequestProperty("Content-type", "application/json");
//                    c.setUseCaches(false);
//                    c.setAllowUserInteraction(false);
//                    c.connect();
//                    int status = c.getResponseCode();
//
//                    switch (status) {
//                        case 200:
//                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
//                            StringBuilder sb = new StringBuilder();
//                            String line;
//                            while ((line = br.readLine()) != null) {
//                                sb.append(line+"\n");
//                            }
//                            br.close();
//                            handlerToast(sb.toString());
//                            Log.d("ACI2", sb.toString());
//                    }
//
//                } catch (MalformedURLException ex) {
//                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//                } catch (IOException ex) {
//                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//                } finally {
//                    if (c != null) {
//                        try {
//                            c.disconnect();
//                        } catch (Exception ex) {
//                            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                }
//            }
//        };
//
//        Thread t = new Thread(r);
//        t.start();
//    }
}
