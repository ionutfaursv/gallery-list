package com.example.ionut.androidtutorial;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.sql.Time;

/**
 * Created by Ionut on 6/28/2015.
 */
public class BazaDeDate {

    private DB db;
    public BazaDeDate(Context context){
        db = new DB(context);
    }

    public long insertDB(String name, String text, int image){
        SQLiteDatabase theDB = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(db.NAME, name);
        contentValues.put(db.TEXT, text);
        contentValues.put(db.IMAGE, image);
        long id = theDB.insert(db.TABLE_NAME, null, contentValues);
        return id;
    }

    public String selectAll(int imageID){
        SQLiteDatabase theDB = db.getWritableDatabase();
        String[] select = {db.UID, db.NAME, db.TEXT, db.IMAGE};
        //  (table, select[], section[], sectionArgs[], groupBy, having, orderBy)
        String[] selectionArgs = {Integer.toString(imageID)};
        Cursor cursor = theDB.query(db.TABLE_NAME, select, db.IMAGE + " = ? ", selectionArgs, null, null, db.UID + " DESC");
        StringBuffer buffer = new StringBuffer();

        while(cursor.moveToNext()){
            int index1 = cursor.getColumnIndex(db.UID);
            int index2 = cursor.getColumnIndex(db.NAME);
            int index3 = cursor.getColumnIndex(db.TEXT);
            int index4 = cursor.getColumnIndex(db.IMAGE);

            int id = cursor.getInt(index1);
            String name = cursor.getString(index2);
            String text = cursor.getString(index3);
            int image = cursor.getInt(index4);

            buffer.append(id + "  |  " + name + "  |  " + text + "  |  " + image + "\n");
        }
        return buffer.toString();
    }

    public String getTheText(String name) {
        SQLiteDatabase theDB = db.getWritableDatabase();
        String[] select = {db.TEXT};
        String[] selectionArgs = {name};
        //  (table, select[], section[], sectionArgs[], groupBy, having, orderBy)
        Cursor cursor = theDB.query(db.TABLE_NAME, select, db.NAME + " = ?", selectionArgs, null, null, null);
        StringBuffer buffer = new StringBuffer();

        while(cursor.moveToNext()){
            int index3 = cursor.getColumnIndex(db.TEXT);
            String text = cursor.getString(index3);

            buffer.append(text+ "\n");
        }
        return buffer.toString();
    }

    public int deleteRow(){
        SQLiteDatabase theDB = db.getWritableDatabase();
        String[] name = {""};
        int restult = theDB.delete(db.TABLE_NAME, db.NAME + " = ?", name);
        return restult;
    }

    public int updateRow(){
        SQLiteDatabase theDB = db.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(db.NAME, "Ghost");
        String[] name = {""};
        int restult = theDB.update(db.TABLE_NAME, cv,db.NAME + " = ?", name);
        return restult;
    }

    static class DB extends SQLiteOpenHelper {
        private static final String DB_NAME = "dbtest";
        private static final String TABLE_NAME = "USERS";
        private static final int DB_VERSION = 7;
        private static final String UID = "_id";
        private static final String NAME = "nume";
        private static final String TEXT = "text";
        private static final String IMAGE = "image";
        private static final String CREATE_TABLE = "CREATE TABLE "+ TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ NAME +" VARCHAR (255), " + TEXT + " VARCHAR (255)," + IMAGE + " INTEGER);";
        private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;

        public DB(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            this.context = context;
            Toast.makeText(context, "constructor", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE);
                Toast.makeText(context, "onCreate", Toast.LENGTH_SHORT).show();
            } catch (SQLException e){
                Toast.makeText(context, "onCreate: " +e, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE);
                onCreate(db);
                Toast.makeText(context, "onUpgrade", Toast.LENGTH_SHORT).show();
            } catch (SQLException e){
                Toast.makeText(context, "onUpgrade: " +e, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
