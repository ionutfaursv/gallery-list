package com.example.ionut.androidtutorial.beans;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ionut.androidtutorial.MainActivity;
import com.example.ionut.androidtutorial.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ionutfaur on 08.07.2015.
 */
public class Comment {

    private int id;
    private String fileName;
    private int user;
    private long time;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Comment) {
            return getId() == ((Comment) o).getId();
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return getId();
    }
}
